# Playground

Various tests, usually in [branches]:
- [issue-37986]
- [issue-36208]
- [issue-216906]

[branches]: https://gitlab.com/glensc/playground/-/branches
[issue-37986]: https://gitlab.com/glensc/playground/tree/issue-37986
[issue-36208]: https://gitlab.com/glensc/playground/tree/issue-36208
[issue-216906]: https://gitlab.com/glensc/playground/tree/issue-216906
